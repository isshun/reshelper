import com.mortennobel.imagescaling.experimental.ImprovedMultistepRescaleOp;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

/**
 * Created by Alex on 19/03/2015.
 */
public class Main {
    public static void main(String [] args) {
        if (args.length != 2) {
            System.out.println("Usage: helper.jar resources_directory output_directory");
            return;
        }

        File inputDirectory = new File(args[0]);
        File outputDirectory = new File(args[1]);

        try {
            createDPI("mdpi", inputDirectory, new File(outputDirectory, "drawable-mdpi"), 1);
            createDPI("hdpi", inputDirectory, new File(outputDirectory, "drawable-hdpi"), 1.5);
            createDPI("xhdpi", inputDirectory, new File(outputDirectory, "drawable-xhdpi"), 2);
            createDPI("xxhdpi", inputDirectory, new File(outputDirectory, "drawable-xxhdpi"), 3);
        } catch(IOException e) {
            System.out.println(e.getMessage());
        }
    }

    private static void createDPI(String label, File inputDirectory, File outputDirectory, double ratio) throws IOException {
        inputDirectory.mkdirs();
        outputDirectory.mkdirs();

        for (File file: inputDirectory.listFiles()) {
            BufferedImage image = ImageIO.read(file);

            if (file.getName().endsWith(".9.png")) {
                System.out.println("Ignore " + file.getName());
            }
            else if (file.getName().endsWith(".png")) {
                System.out.println("Resize " + file.getName() + " to " + label);
                BufferedImage resizeImage = resizeImageWithHint(image, ratio);
                ImageIO.write(resizeImage, "png", new File(outputDirectory, file.getName()));
            } else if (file.getName().endsWith(".jpg")) {
                System.out.println("Resize " + file.getName() + " to " + label);
                BufferedImage resizeImage = resizeImageWithHint(image, ratio);
                ImageIO.write(resizeImage, "jpg", new File(outputDirectory, file.getName()));
            } else {
                System.out.println("Ignore " + file.getName());
            }
        }
    }

    private static BufferedImage resizeImageWithHint(BufferedImage image, double ratio) {
        return resizeImageWithHint(image, (int) (image.getWidth() * ratio / 10), (int) (image.getHeight() * ratio / 10));
    }

    private static BufferedImage resizeImageWithHint(BufferedImage image, int width, int height) {
        return new ImprovedMultistepRescaleOp(width, height).filter(image, null);
    }
}
